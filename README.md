# School app

# Setting up the project in WSL

* Clone the repository.
* Create a public key to be able to pull and push ss-keygen.
* Copy the key and add it to your account if necessary.
* sudo apt-get update
* sudo apt-get install python3-pip
* pip3 install virtualenv
* virtualenv environment_name
* Activate it: source venv/bin/activate
* pip3 install django
* python3 manage.py runserver

Happy coding "Code early and code often" MIT Spectrum <br>
Database: PostgreSQL.

# Performing migration

python manage.py makemigrations <br>
python manage.py migrate

After doing this you can create the account to log into the admin panel by using the following command:
python manage.py createsuperuser ---> and follow the prompt
