$(document).ready(function(){
    $('.menu-toggle').on('click', function() {
        $('.nav-wrapper').toggleClass('showing');
        $('.nav-wrapper ul').toggleClass('showing');
    }
    );
});


$(document).ready(function(){
  $(".default_option").on('click', function(){
    $(".dropdown ul").addClass('active');
  });

  $(".dropdown ul li").on('click', function(){
    var text = $(this).text();
    $(".default_option").text(text);
    $(".dropdown ul").removeClass('active');
  });
});
