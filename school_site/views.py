from django.shortcuts import render
from django.http import HttpResponse
from .forms import AdvertForm
from .models import SchoolDetails
# Create your views here.

def index (request):
    data = SchoolDetails.objects.all()
    #print("This is the data: -----------------------------: ",data.title)
    return render(request, 'index.html', {'result':data})


def posted (request):
    data = SchoolDetails.objects.all()

    return render(request, "post.html", {'result':data})

def advert_form (request):
    
    form = AdvertForm()
    if request.method == "POST":
        form = AdvertForm(request.POST)
        print(form.errors)
        if form.is_valid():
            print(request.POST)
            form.save()

        #post = form.save(commit=False)
        #post.user = request.user
        #post.save()
        #form = AdvertForm()
    #form = AdvertForm()
    return render(request, 'advert.html', {'form': form})

def about (request):
    data = SchoolDetails.objects.all()
    return render(request, 'about.html', {'result':data})

def home (request):
    data = SchoolDetails.objects.all()
    #print("This is the data: -----------------------------: ",data.title)
    return render(request, 'index.html', {'result':data})

def help (request):
    return render(request, 'help.html')

def contact (request):
    return render (request, 'contact.html')
