from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class SchoolDetails(models.Model):
    school_name = models.CharField(max_length = 120)
    title = models.CharField(max_length = 120)
    category = models.CharField(max_length = 120)
    description = models.TextField()
    email = models.EmailField(max_length = 200) 
    phone = models.CharField(max_length = 120)
    deadline = models.CharField(max_length = 120)
    #logo = models.ImageField()
    contact_person = models.CharField(max_length = 120)
    province= models.CharField(max_length = 120)
    additional_info = models.CharField(max_length = 120)
    country = models.CharField(max_length = 120)

# class Post(models.Model):
#     school_name = models.CharField(max_length = 120)
#     title = models.CharField(max_length = 120)
#     category = models.CharField(max_length = 120)
#     description = models.TextField()
#     email = models.EmailField(max_length = 200) 
#     phone = models.CharField(max_length = 120)
#     deadline = models.CharField(max_length = 120)
#     logo = models.ImageField()
#     contact_person = models.CharField(max_length = 120)
#     province= models.CharField(max_length = 120)
#     additional_info = models.CharField(max_length = 120)
#     country = models.CharField(max_length = 120)
#     user = models.ForeignKey(User, on_delete=True)


    