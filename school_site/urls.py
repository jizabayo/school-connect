from django.urls import path
from . import views
from .models import SchoolDetails

urlpatterns = [
    path('', views.index, name='index'),
    path('home', views.home, name='home'),
    path('advert', views.advert_form, name='advert'),
    path('posted', views.posted, name='posted'),
    path('about', views.about, name='About'),
    path('help', views.help, name='help'),
    path('contact', views.contact, name='contact')
]